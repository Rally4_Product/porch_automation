package Executable_Classes;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities_Classes.Utility;


public class Add_List 
{
	Utilities_Classes.Utility utility = new Utilities_Classes.Utility();
	Functional_Classes.Functions functions = new Functional_Classes.Functions();
	public Utilities_Classes.RepositoryParser parser;
	public static String TestDB = "Porch.porchtestingdata";
	String Cases,Execute;
	static WebDriver driver;
	
	static String REcolorado,IRES, streetaddress,city,zipco,subare,date,price,clientemail,verificationList;
	
	@Test(priority=7)
	public void GetData() throws SQLException, ClassNotFoundException, IOException
	{
		parser = new Utilities_Classes.RepositoryParser("project.properties");
		driver = Login_Application.driver;
		Connection Conn = Utility.OpenConnenctionTestData();
		Statement stmt=Conn.createStatement();  
		ResultSet rs=stmt.executeQuery("SELECT Cases,Recolorado,IRES,streetaddress,city,zipcode,subarea,date,price,clientemail  FROM "+TestDB+" WHERE Cases = 'Add_List'" );  

		while(rs.next())  
		{
			Cases = rs.getString(1);
			REcolorado = rs.getString(2);
			IRES = rs.getString(3);
			streetaddress = rs.getString(4);
			city = rs.getString(5);
			zipco = rs.getString(6);
			subare = rs.getString(7);
			date = rs.getString(8);
			price = rs.getString(9);
			clientemail = rs.getString(10);
			verificationList = streetaddress+" "+city+", "+subare+" "+zipco; 
		}

		stmt.close();
		Conn.close();
	}
	
	
	@Test(priority=8)
	public void Open_Hoses_Schedule()  throws InterruptedException 
	{
		functions.SideMenuList(driver, "Open Houses" ,"Schedule" ,parser);
	}
	
	@Test(priority=9)
	public void Add_Listing_Button() throws InterruptedException
	{
		functions.AddListingButton(driver, parser);
	}
	
	@Test(priority=10)
	public void Add_REColoradoMLS() throws InterruptedException
	{
		functions.Add_REColoradoMLS(driver, REcolorado, parser);		
	}
	
	@Test(priority=11)
	public void Add_IRESMLS() throws InterruptedException
	{
		functions.Add_IRESMLS(driver, IRES, parser);
	}
	
	@Test(priority=12)
	public void Add_StreesAddress() throws InterruptedException
	{
		functions.Add_Streetaddress(driver, streetaddress, parser);	
	}
	
	@Test(priority=13)
	public void Add_City() throws InterruptedException
	{
		functions.Add_City(driver, city, parser);
	}
	
	@Test(priority=14)
	public void Add_Zipcode() throws InterruptedException
	{
		functions.Add_zipcode(driver, zipco, parser);
	}
	
	@Test(priority=15)
	public void Add_SubArea() throws InterruptedException
	{
		functions.Add_subarea(driver, subare, parser);
	}
	
	@Test(priority=16)
	public void Add_calendarDate() throws InterruptedException
	{
		functions.Add_calendardate(driver, date, parser);
	}
	
	@Test(priority=17)
	public void Add_Price() throws InterruptedException
	{
		functions.Add_price(driver, price, parser);
	}
	
	@Test(priority=18)
	public void Add_ClientEmail() throws InterruptedException
	{
		functions.Add_clientemail(driver, clientemail, parser);
	}
	
	@Test(priority=19)
	public void Create_List() throws InterruptedException
	{
		functions.CreateButton(driver, parser);
	}
	
	@Test(priority=20)
	public void Search_List() throws InterruptedException
	{
		functions.SearchList(driver, streetaddress, parser);
	}
	
	@Test(priority=21)
	public void Verify_List() throws InterruptedException
	{
		functions.Added_List_Verification(driver, verificationList, parser);
	}
	
	
}
