package Executable_Classes;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities_Classes.Utility;


public class Login_Application 
{
	Utilities_Classes.Utility utility = new Utilities_Classes.Utility();
	Functional_Classes.Functions functions = new Functional_Classes.Functions();
	public Utilities_Classes.RepositoryParser parser;
	public static String TestDB = "Porch.porchtestingdata";
	int StartCaseNo = 1;
	String Cases,Execute;
	static WebDriver driver;
	
	static String UserName,Password,URL;
	
	@Test(priority=1)
	@Parameters("browser")
	public void OpenBrowser(String browser) throws IOException 
	{
		parser = new Utilities_Classes.RepositoryParser("project.properties");
		driver = utility.startBrowser(browser);
		
	}
	
	@Test(priority=2)
	public void GetData() throws SQLException, ClassNotFoundException
	{
		Connection Conn = Utility.OpenConnenctionTestData();
		Statement stmt=Conn.createStatement();  
		ResultSet rs=stmt.executeQuery("SELECT Cases,URL,UserName,Password  FROM "+TestDB+" WHERE Cases = 'LoginId' ");  

		while(rs.next())  
		{
			Cases = rs.getString(1);
			URL = rs.getString(2);
			UserName = rs.getString(3);
			Password = rs.getString(4);
			
		}

		stmt.close();
		Conn.close();
		StartCaseNo++;
		
		
	}
	
	@Test(priority=3)
	public void Open_URL() throws ClassNotFoundException, SQLException 
	{
		functions.Enter_URL(driver, URL);
	}

	@Test(priority=4)
	public void Click_GoogleButton() throws ClassNotFoundException, SQLException, InterruptedException 
	{
		functions.ClickOnGoogleButton(driver, parser);
	}
	
	@Test(priority=5)
	public void Enter_UserName() throws InterruptedException, ClassNotFoundException, SQLException 
	{
		functions.Enter_UserName(driver, UserName,parser);
	}
	
	@Test(priority=6)
	public void Enter_Password()  throws InterruptedException, ClassNotFoundException, SQLException 
	{
		functions.Enter_Password(driver, Password,parser);
	}
	
	
}
