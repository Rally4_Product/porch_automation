package Executable_Classes;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Utilities_Classes.Utility;


public class Logout_Application 
{
	Utilities_Classes.Utility utility = new Utilities_Classes.Utility();
	Functional_Classes.Functions functions = new Functional_Classes.Functions();
	public Utilities_Classes.RepositoryParser parser;
	int StartCaseNo = 1;
	String Cases,Execute;
	static WebDriver driver;
	
	static String UserName,Password,URL;
	
	@Test(priority=22)
	public void Logout() throws InterruptedException, IOException
	{
		parser = new Utilities_Classes.RepositoryParser("project.properties");
		driver = Login_Application.driver;
		functions.Logout(driver, parser);
	}
	
	@Test(priority=23)
	public void Close_Browser() 
	{
		driver.quit();
	}
	
	
}
