package Functional_Classes;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Executable_Classes.Add_List;
import Utilities_Classes.RepositoryParser;
import Utilities_Classes.Utility;

public class Functions {

	String TestDB = Add_List.TestDB;
	Utilities_Classes.RepositoryParser parser;		
	public void Enter_URL(WebDriver driver, String loginURL) throws SQLException, ClassNotFoundException
	{
		String URL = loginURL;
		driver.get(URL);
		
	}
	
	public void Enter_UserName(WebDriver driver, String Name,RepositoryParser parser) throws InterruptedException, SQLException, ClassNotFoundException
	{
		String UserName = Name;
				
		driver.findElement(parser.getObjectLocator("LoginId")).sendKeys(UserName);
		Thread.sleep(1000);
		driver.findElement(parser.getObjectLocator("LoginNextButton")).click();
		Thread.sleep(7000);
		
	}
	
	public void Enter_Password(WebDriver driver, String Pass,RepositoryParser parser) throws InterruptedException, SQLException, ClassNotFoundException
	{
		String Password = Pass;
		driver.findElement(parser.getObjectLocator("password")).sendKeys(Password);
		Thread.sleep(3000);
		driver.findElement(By.id("passwordNext")).click();
		Thread.sleep(8000);
		driver.findElement(By.id("yDmH0d")).click();
		Thread.sleep(8000);
		driver.findElement(By.id("submit_approve_access")).click();
		Thread.sleep(12000);
				
	}
	
	public void Login_Button(WebDriver driver,RepositoryParser parser) throws InterruptedException
	{
		driver.findElement(parser.getObjectLocator("LoginButton")).click();
		Thread.sleep(5000);
	}
	
	public void ClickOnGoogleButton(WebDriver driver,RepositoryParser parser) throws InterruptedException
	{
		WebElement googlebutton = driver.findElement(parser.getObjectLocator("googlebutton"));
		googlebutton.click();
		Thread.sleep(5000);
	}
	
	public void SideMenuList(WebDriver driver,String MenuName, String SubMenu, RepositoryParser parser) throws InterruptedException
	{
		Thread.sleep(1000);
		List<WebElement> MenuList = driver.findElements(parser.getObjectLocator("LeftMenuList"));
		Thread.sleep(1000);
		outerloop :
		for(int nextmenu = 0; nextmenu<MenuList.size(); nextmenu++)
		{
			if(MenuList.get(nextmenu).getText().equalsIgnoreCase(MenuName))
			{
				MenuList.get(nextmenu).click();
				Thread.sleep(1000);	
				List<WebElement> subMenuList = MenuList.get(nextmenu).findElements(By.xpath("ul/li"));
				Thread.sleep(1000);
				for(int nextsubmenu = 0; nextsubmenu<subMenuList.size(); nextsubmenu++)
				{
					if(subMenuList.get(nextsubmenu).getText().equalsIgnoreCase(SubMenu))
					{
						subMenuList.get(nextsubmenu).click();
						Thread.sleep(3000);
						break outerloop;
					}
						
				}
			}
		}
		
	}
	public void AddListingButton(WebDriver driver,RepositoryParser parser) throws InterruptedException
	{
		WebElement googlebutton = driver.findElement(parser.getObjectLocator("AddListingButton"));
		googlebutton.click();
		Thread.sleep(4000);
	}
	
	public void Add_REColoradoMLS(WebDriver driver, String REColo,RepositoryParser parser) throws InterruptedException
	{
		WebElement REColorado = driver.findElement(parser.getObjectLocator("REColoradoMLS"));
		REColorado.sendKeys(REColo);
		Thread.sleep(1000);
	}
	
	public void Add_IRESMLS(WebDriver driver, String IRES,RepositoryParser parser) throws InterruptedException
	{
		WebElement IRESMLS = driver.findElement(parser.getObjectLocator("IRESMLS"));
		IRESMLS.sendKeys(IRES);
		Thread.sleep(1000);
	}
	
	public void Add_Streetaddress(WebDriver driver, String StreetAddress,RepositoryParser parser) throws InterruptedException
	{
		WebElement address = driver.findElement(parser.getObjectLocator("streetaddress"));
		address.click();
		Thread.sleep(1000);
		address.sendKeys(StreetAddress);
		Thread.sleep(1000);
	}
	
	public void Add_City(WebDriver driver, String city,RepositoryParser parser) throws InterruptedException
	{
		WebElement addcity = driver.findElement(parser.getObjectLocator("city"));
		addcity.sendKeys(city);
		Thread.sleep(1000);
	}
	
	public void Add_zipcode(WebDriver driver, String zipcode,RepositoryParser parser) throws InterruptedException
	{
		WebElement addzipcode = driver.findElement(parser.getObjectLocator("zipcode"));
		addzipcode.sendKeys(zipcode);
		Thread.sleep(1000);
	}
	
	public void Add_subarea(WebDriver driver, String subarea,RepositoryParser parser) throws InterruptedException
	{
		WebElement addsubarea = driver.findElement(parser.getObjectLocator("subarea"));
		addsubarea.sendKeys(subarea);
		Thread.sleep(1000);
	}
	
	public void Add_calendardate(WebDriver driver, String calendardate,RepositoryParser parser) throws InterruptedException
	{
		WebElement addcalendardate = driver.findElement(parser.getObjectLocator("calendardate"));
		addcalendardate.clear();
		addcalendardate.sendKeys(calendardate);
		Thread.sleep(1000);
		addcalendardate.sendKeys(Keys.TAB);
		Thread.sleep(1000);
	}
	
	public void Add_price(WebDriver driver, String price,RepositoryParser parser) throws InterruptedException
	{
		WebElement addprice = driver.findElement(parser.getObjectLocator("price"));
		addprice.sendKeys(price);
		Thread.sleep(1000);
	}
	
	public void Add_clientemail(WebDriver driver, String clientemail,RepositoryParser parser) throws InterruptedException
	{
		WebElement addclientemail = driver.findElement(parser.getObjectLocator("clientemail"));
		addclientemail.sendKeys(clientemail);
		Thread.sleep(2000);
	}
	
	public void CreateButton(WebDriver driver, RepositoryParser parser) throws InterruptedException
	{
		WebElement createButton = driver.findElement(parser.getObjectLocator("createButton"));
		createButton.click();
		try
		{
			createButton.click();
		}catch (Exception ex) {}
		
		Thread.sleep(10000);
	}
	
	public void SearchList(WebDriver driver, String ListName,RepositoryParser parser) throws InterruptedException
	{
		WebElement Search_List = driver.findElement(parser.getObjectLocator("searchtextarea"));
		Search_List.sendKeys(ListName);
		Thread.sleep(5000);
	}
	
	public void Added_List_Verification(WebDriver driver, String ListName,RepositoryParser parser) throws InterruptedException
	{
		String AddedListName = driver.findElement(parser.getObjectLocator("AddedListName")).getText();
		if(AddedListName.equalsIgnoreCase(ListName))
		{
			System.out.println("Pass, List "+ListName+" is Added...!!!");
		}
		Thread.sleep(1000);
	}
	
	public void Logout(WebDriver driver,RepositoryParser parser) throws InterruptedException
	{
		driver.findElement(parser.getObjectLocator("Logout")).click();
		Thread.sleep(2000);
	}
}
