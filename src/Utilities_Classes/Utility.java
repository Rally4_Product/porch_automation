package Utilities_Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import Executable_Classes.Login_Application;

public class Utility {
	static WebDriver driver = null;
	static String DatabaseIP = "localhost";
	static String Databaseport = "3306";
	static String DBUser = "root";
	static String DBPass = "Test@1234";
	public WebDriver startBrowser(String BrowserName)
	{
		driver = null;
		
		if(BrowserName.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\RequiredFiles\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		else if(BrowserName.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\RequiredFiles\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(BrowserName.equalsIgnoreCase("ie"))
		{
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\RequiredFiles\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		} 
		
		driver.manage().window().maximize();
		return driver;
	}
	
	public static Connection OpenConnenctionTestData() throws ClassNotFoundException, SQLException
	{
		Connection Conn = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");  
			Conn=DriverManager.getConnection("jdbc:mysql://"+DatabaseIP+":"+Databaseport+"",DBUser,DBPass);			
		}
		 catch(Exception ex)
		{
			 System.out.println(ex);
		}
		return Conn;
	}
}
